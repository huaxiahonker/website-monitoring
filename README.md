## 当前版本：
V0.1
## 开发语言：
c# .netframwork
## 实现功能：
1、资产可用性监测
2、报告导出
3、日志记录
## 发行版下载：
https://gitee.com/huaxiahonker/website-monitoring/releases/tag/V0.1
## 主界面：
![输入图片说明](z.jpg)
## 监测状态：
![输入图片说明](j.jpg)
## 监测报告：
通过查询数据后选择导出报告方式，报告模板文件在template 文件夹下面
![输入图片说明](t.jpg)