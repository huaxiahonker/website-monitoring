﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace website_monitor
{
    public class Email
    {
        /// <summary>
        /// 发送email
        /// </summary>
        /// <param name="sendtoEmail">邮件接收者地址</param>
        /// <param name="title">邮件标题</param>
        /// <param name="htmlInfo">邮件内容</param>
        /// <param name="sendfromEmail">发件人email地址 例如："uewang@gmail.com</param>
        /// <param name="sendfromPwd">发件人密码(或者为授权码,二级密码等)</param>
        /// <param name="smtp">发送邮件smtp服务器(用不同的邮箱，stmp定义则不同) 例如："smtp.gmail.com",注意：有的smtp服务器需要在邮箱中设置可以使用smtp服务</param>
        /// <param name="CCAddressList">邮件抄送列表(可显示地址)</param>
        /// <param name="BccAddressList">邮件秘密抄送列表(不显示地址)</param>
        /// <param name="postNum">post端口号 通常都是25</param>
        /// <param name="fromUsername">发件人用户名(默认会从sendfromEmail取值) 例如："uewang@gmail.com"中的uewang</param>
        /// <param name="attachmentList">要上传的附件物理路径集合</param>
        /// <returns></returns>
        public static bool MailSend(string sendtoEmail, string title, string htmlInfo,
            string sendfromEmail, string sendfromPwd, string smtp,
            List<string> CCAddressList = null, List<string> BccAddressList = null,
            int postNum = 25, string fromUsername = "", List<string> attachmentList = null)
        {
            //邮件内容
            MailMessage myMail = new MailMessage();
            myMail.From = new MailAddress(sendfromEmail);
            myMail.To.Add(new MailAddress(sendtoEmail));
            myMail.Subject = title;
            myMail.SubjectEncoding = System.Text.Encoding.UTF8;
            myMail.Body = htmlInfo;
            myMail.BodyEncoding = System.Text.Encoding.UTF8;
            myMail.IsBodyHtml = true;
            myMail.Priority = MailPriority.High;
            //附件
            if (attachmentList != null && attachmentList.Count > 0)
            {
                foreach (var attachment in attachmentList)
                {
                    myMail.Attachments.Add(new Attachment(attachment));
                }
            }



            if (CCAddressList != null && CCAddressList.Count > 0) //抄送
            {
                foreach (var ea in CCAddressList)
                {
                    myMail.CC.Add(new MailAddress(ea));
                }
            }

            if (BccAddressList != null && BccAddressList.Count > 0) //密抄送
            {
                foreach (var ea in BccAddressList)
                {
                    myMail.Bcc.Add(new MailAddress(ea));
                }
            }

            if (fromUsername.Length == 0) fromUsername = sendfromEmail.Split('@')[0];

            //smtp 服务器
            SmtpClient smtpCt = new SmtpClient();
            smtpCt.UseDefaultCredentials = true;
            smtpCt.Host = "smtp.163.com";//发件人邮件服务器
            smtpCt.Port = 25;//端口,如果不设置的情况下  默认端口就是25
            smtpCt.Credentials = new System.Net.NetworkCredential("usernmae", "shouquanma");
            smtpCt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpCt.EnableSsl = false;
            try
            {
                smtpCt.Send(myMail);
                //Console.WriteLine("ok");
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("error" + ex);
                return false;
            }
        }
    }
}
