﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace website_monitor
{
    public partial class ReportSet : Form
    {
        public ReportSet()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == string.Empty && textBox2.Text == string.Empty)
            {
                Console.WriteLine("数据不能为空！");
                MessageBox.Show("数据不能为空！");
                return;
            }
            //把数据更新到数据库
            try
            {
                // 读取数据的方法
                string path = "data source=Sec-montior.db";
                SQLiteConnection dataConn = new SQLiteConnection(path);
                dataConn.Open();
                //dataConn.EnableExtensions(true);
                //dataConn.LoadExtension("System.Data.SQLite.dll", "sqlite3_fts5_init");
                if (dataConn.State == ConnectionState.Open)
                {
                    //如果数据库能打开的情况下 把数据存入到数据库中
                    //string sql = "UPDATE Sec_reportset SET (username, repname, time) VALUES('{0}', '{1}', '{2}')";
                    string sql = "update Sec_reportset set username = '" + textBox1.Text + "', repname = '" + textBox2.Text + "', addtime = '" + dateTimePicker1.Text + "'";
                    // repname = "+ textBox2.Text + " addtime = "+ dateTimePicker1.Text +"
                    //sql = string.Format(sql,textBox1.Text,textBox2.Text,dateTimePicker1.Text);
                    SQLiteCommand command = new SQLiteCommand(sql, dataConn);
                    command.ExecuteNonQuery();
                    SQLiteDataAdapter mAdapt = new SQLiteDataAdapter(command);
                    var ss = mAdapt.ToString();
                    //判断是否进行执行 如果执行之后就进行打印
                    if (ss.Length != 1)
                    {
                        Console.WriteLine(DateTime.Now + "   ------   " + "配置保存成功" + "\r\n");
                        MessageBox.Show("配置保存成功！" + "\r\n" + "修改时间：" + DateTime.Now);
                        //修改成功后把以下属性设置flase，只有点击修改的时候方可进行修改
                        textBox1.Enabled = false;
                        textBox2.Enabled = false;
                        dateTimePicker1.Enabled = false;
                        return;
                    }
                }
                else
                {
                    //进行日志记录
                    AppLog.WriteError("发现时间：" + DateTime.Now, true);
                    MessageBox.Show("配置保存失败！" + "\r\n" + "修改时间：" + DateTime.Now);
                    //进行打印输出  循环绑定到定时器上 执行N次
                    Console.WriteLine(DateTime.Now + "   ------   " + "配置保存失败" + "\r\n");
                    return;
                }
            }
            catch (Exception ex)
            {
                //记录错误日志 这个日志不会同步到数据库 经过测试 上方因为是实时往数据库中进行存放的数据 在对设备性能上面有一定的要求
                MessageBox.Show("配置保存失败！" + "\r\n" + "修改时间：" + DateTime.Now);
                AppLog.WriteError("错误时间" + DateTime.Now + "\r\n" + ex, true);
                Console.WriteLine("错误时间" + DateTime.Now + "\r\n" + ex);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //点击修改的时候，首先对设置的属性还原
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            dateTimePicker1.Enabled = true;
        }

        private void ReportSet_Load(object sender, EventArgs e)
        {
            try
            {
                // 读取数据的方法
                string path = "data source=Sec-montior.db";
                SQLiteConnection dataConn = new SQLiteConnection(path);
                dataConn.Open();
                //dataConn.EnableExtensions(true);
                //dataConn.LoadExtension("System.Data.SQLite.dll", "sqlite3_fts5_init");
                if (dataConn.State == ConnectionState.Open)
                {
                    DataSet ds = new DataSet();
                    string sql = "SELECT username, repname, addtime FROM Sec_reportset";
                    SQLiteCommand command = new SQLiteCommand(sql, dataConn);
                    command.ExecuteNonQuery();
                    SQLiteDataAdapter mAdapt = new SQLiteDataAdapter(command);
                    mAdapt.Fill(ds);
                    var ss = mAdapt.ToString();
                    //判断是否进行执行 如果执行之后就进行打印
                    if (ss.Length != 1)
                    {
                        textBox1.Text = ds.Tables[0].Rows[0]["username"].ToString();
                        textBox2.Text = ds.Tables[0].Rows[0]["repname"].ToString();
                        dateTimePicker1.Text = ds.Tables[0].Rows[0]["addtime"].ToString();
                        Console.WriteLine(DateTime.Now + "   ------   " + "配置加载成功" + "\r\n");
                        dataConn.Close();
                    }
                }
                else
                {
                    //进行日志记录
                    AppLog.WriteError("发现时间：" + DateTime.Now, true);
                    //进行打印输出  循环绑定到定时器上 执行N次
                    Console.WriteLine(DateTime.Now + "   ------   " + "配置保存失败" + "\r\n");
                    return;
                }
            }
            catch (Exception ex)
            {
                //记录错误日志 这个日志不会同步到数据库 经过测试 上方因为是实时往数据库中进行存放的数据 在对设备性能上面有一定的要求
                AppLog.WriteError("错误时间" + DateTime.Now + "\r\n" + ex, true);
                Console.WriteLine("错误时间" + DateTime.Now + "\r\n" + ex);
                return;
            }
        }
    }
}
